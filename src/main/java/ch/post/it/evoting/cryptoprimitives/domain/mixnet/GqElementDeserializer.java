/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.post.it.evoting.cryptoprimitives.domain.mixnet;

import static ch.post.it.evoting.cryptoprimitives.domain.ConversionUtils.base64ToBigInteger;
import static ch.post.it.evoting.cryptoprimitives.domain.ConversionUtils.hexToBigInteger;

import java.io.IOException;
import java.math.BigInteger;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Deserializes a json to a {@link GqElement}.
 */
class GqElementDeserializer extends JsonDeserializer<GqElement> {

	/**
	 * The {@code context} must provide the {@link GqGroup} that will be used to reconstruct the various {@link GqElement}s.
	 * <p>
	 * {@inheritDoc}
	 */
	@Override
	public GqElement deserialize(final JsonParser parser, final DeserializationContext context) throws IOException {
		final GqGroup gqGroup = (GqGroup) context.getAttribute("group");
		final Boolean base64Conversion = (Boolean) context.getAttribute("base64Conversion");

		final JsonNode node = new ObjectMapper().readTree(parser);
		final String value = node.asText();

		BigInteger bigIntegerValue;
		if (base64Conversion != null && base64Conversion) {
			bigIntegerValue = base64ToBigInteger(value);
		} else {
			bigIntegerValue = hexToBigInteger(value);
		}

		return GqElement.GqElementFactory.fromValue(bigIntegerValue, gqGroup);
	}

}
