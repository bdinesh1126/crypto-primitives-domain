# Crypto-Primitives-Domain Swiss Post

---

Crypto-Primitives-Domain
Copyright 2022 Post CH Ltd.

The crypto-primitives-domain library encapsulates data objects of the Swiss Post Voting System developed at Post CH Ltd.

The crypto-primitives-domain library is E-Voting Community Program Material - please follow our Code of Conduct describing what you can expect from us, the Coordinated Vulnerability Disclosure Policy, and the contributing guidelines.
